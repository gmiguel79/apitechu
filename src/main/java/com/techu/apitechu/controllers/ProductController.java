package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts () {
        System.out.println("getProducts");
        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {

        System.out.println("getProductById");
        System.out.println("id es: " + id);
        ProductModel result = new ProductModel();

        for (ProductModel product: ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                result = product;
            }

        }
        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("El id de producto es: " + newProduct.getId());
        System.out.println("La descripcion de producto es: " + newProduct.getDesc());
        System.out.println("El precio es: " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@PathVariable String id, @RequestBody ProductModel product){
        System.out.println("updateProduct");
        System.out.println("El id path param es: " + id);
        System.out.println("Body: El id de producto es: " + product.getId());
        System.out.println("Body: La descripcion de producto es: " + product.getDesc());
        System.out.println("Body: El precio es: " + product.getPrice());

        for (ProductModel productInList: ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("El id de pathParam es: " +id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                result = product;
                break;
            }
        }
        ApitechuApplication.productModels.remove(result);
        return result;

    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel modifyProduct(@PathVariable String id, @RequestBody ProductModel product) {
        //id no se puede actualizar, desc si y precio si
        System.out.println("modifyProduct");
        System.out.println("El id de pathParam es: " +id);
        System.out.println(product.getDesc());
        System.out.println(product.getPrice());

        ProductModel result = new ProductModel();


        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                if (product.getPrice() > 0.0) {
                    productInList.setPrice(product.getPrice());
                    System.out.println("Actualización de precio.");
                }
                if (product.getDesc() != null) {
                    System.out.println("Actualización de descripcion.");
                    productInList.setDesc(product.getDesc());
                }
                result = productInList;
                break;
            }
        }
        return result;
    }
}

