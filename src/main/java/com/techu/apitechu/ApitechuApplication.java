package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController

public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechuApplication.class, args);
		ApitechuApplication.productModels = ApitechuApplication.getTestData();
	}
	/*@RequestMapping("/hello")
	public String greetings(){
		return "Hola mundo desde API Tech U";
	}
	*/

	private static ArrayList<ProductModel> getTestData(){
		ArrayList<ProductModel> productModels = new ArrayList<>();
		productModels.add(new ProductModel("1","Pr 1", 10));
		productModels.add(new ProductModel("2","Pr 2", 20));
		productModels.add(new ProductModel("3","Pr 3", 30));
		productModels.add(new ProductModel("4","Pr 4", 40));
		return productModels;
	}
}
